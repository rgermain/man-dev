# Bash

## Declaration

### Function

```bash
function name () {
    ...
}
# or
name () {
    ...
}
```

### Variable

```bash
name=value
```

## Regex

| sassign  | description                                   |
| -------- | --------------------------------------------- |
| [a-zA-Z] | only alphabetical char in lower ou upper case |
| \*       | match anything anytime                        |
| ?        | match anything once                           |

no space between "="
