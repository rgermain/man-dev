import sys

# Function for nth Fibonacci number
def Fibonacci(n):
   
    # Check if input is 0 then it will
    # print incorrect input
    if n < 0:
        print("Incorrect input")
 
    # Check if n is 0
    # then it will return 0
    elif n == 0:
        return 0
 
    # Check if n is 1,2
    # it will return 1
    elif n == 1 or n == 2:
        return 1
 
    else:
        return Fibonacci(n-1) + Fibonacci(n-2)
 

def fibonacy(nb):
    f = [0,1,0]
    if nb < 0:
        raise ValueError("nb is negative")
    elif nb < 2:
       return 1
    while nb:
        nb -= 1
        f[2] = f[0] + f[1]
        f[0] = f[1]
        f[1] = f[2]
    return f[1]

if __name__ == "__main__":
    print(f"fib: {fibonacy(int(sys.argv[1]))}")
