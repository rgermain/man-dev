fn fib(nb: u128) -> i128 {
    let mut vec: Vec<i128> = Vec::new();
    let mut i: u128 = 0;

    vec.push(0);
    vec.push(1);

    while i < nb {
        let t = vec[0] + vec[1];
        vec[0] = vec[1];
        vec[1] = t;
        i += 1;
    }
    return vec[1];
}

fn main() {
    let r = fib(30);
    println!("nb: {}", r);
}
