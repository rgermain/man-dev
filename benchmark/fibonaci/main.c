#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdint.h>
#include <fcntl.h>
#include <stdbool.h>

typedef struct s_num {
    uint32_t shift;
    long double value;
} t_num;

uint32_t calc_shift(long double val) {
    uint64_t n = abs((uint64_t)val);
    bool neg = val < 0;
    uint32_t i = 0;
    while (n >= 10) {
        i++;
        n /= 10;
    }
    return i;
}

t_num mult(t_num num, long double val) {
    t_num res;
    uint32_t nshift = calc_shift(val);


    return res
}

long double fib(int nb) {
     long double f[3] = {0.0,1.0,0.0};
     if (nb == 0) {
         return 0;
     } else if (nb < 0) {
         return -1;
     }
     while (nb--) {

         f[3] = f[0] + f[1];
         f[0] = f[1];
         f[1] = f[3];
     }
     return f[1];
}


int main(int argc, char **argv){
    if (argc != 2) {
        dprintf(2, "you need one arguments\n");
        return 1;
    }
    long double nb = fib(atoi(argv[1]));
    int fd = open(".nb", O_WRONLY | O_CREAT);
    if (fd < 0){
        dprintf(2, "error write file\n");
        return 1;
    }
    while (nb > 1.0) {
        char buff[50];
        uint8_t size = 0;
        while (size < 50 && nb > 1.0) {
            long double t = nb / 10.0L;
            buff[size] = '0' + (char)(nb - (t * 10.0L));
            nb = t;
            size++;
        }
        write(fd, buff, size);
    }
    close(fd);
    printf("fib [ %s ]: ", argv[1]);
    fd = open(".nb", O_RDONLY);
    if (fd <0 ) {
        dprintf(2, "cant open file");
        return 1;
    }
    char *buff[50];
    int size = 0;
    while (1) {
        int t = read(fd, buff, 50);
        size += t;
        if (t < 50)
            break;
    }
    close(fd);
    fd = open(".nb", O_RDONLY);
    if (fd <0 ) {
        dprintf(2, "cant re-open file");
        return 1;
    }
    char *str = (char*)malloc(sizeof(char) * (size + 1));
    read(fd, str, size);
    close(fd);
    str[size] = 0;
    int s = size - 1;
    int ss = 0;
    while (s > ss) {
        char t = str[s];
        str[s] = str[ss];
        str[ss] = t;
        ss++;
        s--;
    }
    printf("%s\n", str);
    return 0;
}
