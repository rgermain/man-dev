## news:
    https://linuxfr.org/
    https://www.theodinproject.com/home
    https://www.fsf.org/community/

## package:
    https://tailwind-elements.com/
    https://github.com/GrabarzUndPartner/nuxt-speedkit/issues

## tuto:
    https://refactoring.guru/design-patterns
    https://devdocs.io/
    https://www.javatpoint.com/
    https://devhints.io/
    https://1loc.dev/

## info:
    https://github.com/GorvGoyl/Clone-Wars
    https://github.com/EbookFoundation/free-programming-books
    https://www.youtube.com/channel/UCIlUBOXnXjxdjmL_atU53kA
    https://dev.to/madza/17-killer-websites-you-should-use-to-increase-productivity-2enk
    https://github.com/danistefanovic/build-your-own-x
    https://dev.to/frolovdev/parsers-for-dummies-23d0
    https://dev.to/yacouri/reactjs-folder-structure-boilerplate-155n
    https://towardsdatascience.com/20-python-interview-questions-to-challenge-your-knowledge-cddc842297c5
    https://www.youtube.com/c/It-connectFr/videos
    https://zestedesavoir.com/articles/97/introduction-a-la-retroingenierie-de-binaires/
    https://zestedesavoir.com/articles/1652/quelques-rouages-dun-moteur-javascript/
    https://www.elprocus.com/at-commands-tutorial/
    https://www.engineersgarage.com/at-commands-gsm-at-command-set/
    https://albertomosconi.github.io/foss-apps/
    https://www.technologuepro.com/gsm/commande_at.htm
    https://joshuastrobl.com/2021/09/14/building-an-alternative-ecosystem/
    https://download.schenker-tech.de/package/schenker-via-15-pro-idsvi15pm20/
    https://m.soundcloud.com/michael-az-948266686/recrute-chez-w…22-ans-interview-de-kevin-rambonona-nouvelle-recrue-atypique
    https://github.com/p33r-positron/Le-C-en-Fran-C/tree/main/1-Bases
    https://www.l-expert-comptable.com/a/535027-comment-calculer-le-tmj-d-un-freelance-informatique.html
    https://www.smashingmagazine.com/2022/03/new-css-features-2022/
    https://github.com/p33r-positron/Le-C-en-Fran-C
    https://madaidans-insecurities.github.io/linux.html
    https://www.linusakesson.net/programming/tty/
    https://blog.gauravthakur.in/how-javascript-array-works-internally
    https://javascript.info/
    https://gitimmersion.com/lab_01.html

## desing
    https://colourcontrast.cc/
    https://www.reddit.com/r/web_design/comments/uq18cp/heres_a_list_of_free_platforms_you_need_for_your/

## demo:
    https://github.com/yurkagon/Doom-Nukem-CSS
    http://madebyevan.com/webgl-water/
    https://paveldogreat.github.io/WebGL-Fluid-Simulation/
    https://codepen.io/amit_sheen/pen/QWGjRKR
    https://codepen.io/RTarson/pen/yvyvmq
    
## hum:
    https://motherfuckingwebsite.com/
    